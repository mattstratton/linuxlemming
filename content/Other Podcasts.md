+++
date = "2017-04-27T15:18:55-05:00"
title = "Podcasts"

+++

### [Linux Unplugged](https://www.jupiterbroadcasting.com/show/linuxun/)
A great virtual Usergroup for those interested and experienced with linux. 


### [Selfhosted](https://www.jupiterbroadcasting.com/show/self-hosted/)
A podcast focused on self-hosting from the Jupiter Broadcasting network.  


### [Ask Noah Show](http://www.asknoahshow.com/)
A weekly call in show that digs into how to do all the things "they" said couldn't be done on linux **and** teach you how to do the same.


### [Destination Linux](https://destinationlinux.org/)
A quartet of experienced linux users who provide weekly insight into the world of linux and discuss how things like community and advertising can be leveraged to advance the use of linux in business and private usage.

### [Late Night Linux](https://latenightlinux.com/)
A quartet of experienced linux users who provide weekly insight into the world of linux. Hosts are all based in the UK and provide great discussions on many different topics. Geared towards adults and some language may not be appropriate for youth under 18 years of age.